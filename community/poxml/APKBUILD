# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=poxml
pkgver=24.08.0
pkgrel=0
arch="all !armhf !riscv64"
url="https://www.kde.org/applications/development/"
pkgdesc="Translates DocBook XML files using gettext po files"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	gettext-dev
	kdoctools-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-doc"
_repo_url="https://invent.kde.org/sdk/poxml.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/poxml-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1370ec04a05b460564430b7bbc0a34f7ddad69632135bae9a861c22af798c761d228407a3d8dd32e99bbdaa9d67c3c0d13712e8c2ae647469687b5930a85b2fd  poxml-24.08.0.tar.xz
"
