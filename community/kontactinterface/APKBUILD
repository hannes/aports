# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kontactinterface
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org/"
pkgdesc="Kontact Plugin Interface Library"
license="LGPL-2.0-only OR LGPL-3.0-only"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kparts-dev
	kwindowsystem-dev
	kxmlgui-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/kontactinterface.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontactinterface-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5479bb4d21d68dfa72a7e65abf6c41d205df2cd0b11b3ccd74e51bda9ec07cebb7e7f870a340d225468f16ebf35972dbcf47549a603a910b491e3b45b0947889  kontactinterface-24.08.0.tar.xz
"
