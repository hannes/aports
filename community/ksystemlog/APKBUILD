# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ksystemlog
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://apps.kde.org/ksystemlog/"
pkgdesc="KDE SystemLog Application"
license="GPL-2.0-or-later AND BSD-3-Clause"
depends_dev="
	audit-dev
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kitemviews-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-lang $pkgname-doc"
_repo_url="https://invent.kde.org/system/ksystemlog.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ksystemlog-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5ba24fd7fd3f9b0012451fccc3db676004a737bf0401427e26075fff7c357ddd405d9ce6ce6486af88ec8b31d5a1ef89ce939e6ff98ef38fc673e26023a82882  ksystemlog-24.08.0.tar.xz
"
