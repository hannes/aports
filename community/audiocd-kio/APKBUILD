# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=audiocd-kio
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="Kioslave for accessing audio CDs"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	cdparanoia-dev
	extra-cmake-modules
	flac-dev
	kcmutils-dev
	kconfig-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	libkcddb-dev
	libkcompactdisc-dev
	libvorbis-dev
	qt6-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/multimedia/audiocd-kio.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/audiocd-kio-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cf4382799c6657ef6957bf42575ccc61ccab4e10549b78c333bf6801f96b94a9fd47051c853ce9af066082db8e519bb8efade8cbcf9e2d02442ad7cd5f0e71f4  audiocd-kio-24.08.0.tar.xz
"
