# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kfind
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/utilities/kfind"
pkgdesc="Find Files/Folders"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcoreaddons-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kio-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/utilities/kfind.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kfind-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
82e0c60f6fe52ee243f409aee11f904e40a813b177a26bb2b8b35b40ca80339a712c0055e901869d23a659d2bc79e4c14d62044c377e979243aa71f53dbe81d6  kfind-24.08.0.tar.xz
"
