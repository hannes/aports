# Contributor: omni <omni+alpine@hack.org>
# Maintainer: omni <omni+alpine@hack.org>
pkgname=godap
pkgver=2.7.4
pkgrel=0
pkgdesc="A complete TUI for LDAP"
url="https://github.com/Macmod/godap"
license="MIT"
arch="all"
makedepends="go"
subpackages="$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
options="net !check" # no test files
source="$pkgname-$pkgver.tar.gz::https://github.com/Macmod/godap/archive/refs/tags/v$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-ldflags="-linkmode=external -extldflags \"$LDFLAGS\"" \
		-v .

	local shell; for shell in bash fish powershell zsh; do
		./godap completion "$shell" > "$pkgname"_"$shell"
	done
}

package() {
	install -Dm0755 "$pkgname" -t "$pkgdir"/usr/bin

	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/"$pkgname"/LICENSE
	install -Dm644 README.md "$pkgdir"/usr/share/doc/"$pkgname"/README.md

	install -Dm644 "$pkgname"_bash "$pkgdir"/usr/share/bash-completion/completions/"$pkgname"
	install -Dm644 "$pkgname"_fish "$pkgdir"/usr/share/fish/vendor_completions.d/"$pkgname".fish
	install -Dm644 "$pkgname"_zsh "$pkgdir"/usr/share/zsh/site-functions/_"$pkgname"
}

sha512sums="
cc2552b955621c3ecccb315b78c14617a77b351380ea6522c47f7257137f73ffcdaa663b5fca13ecb06cac6f703a6a1a9c5dbf2f38c4a1f48c24dcd921949f76  godap-2.7.4.tar.gz
"
