# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kget
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/internet/org.kde.kget"
pkgdesc="A versatile and user-friendly download manager"
license="GPL-2.0-or-later AND LGPL-2.0-only AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	gpgme-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kitemviews-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kservice-dev
	kstatusnotifieritem-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libktorrent-dev
	libmms-dev
	qca-dev
	qt6-qtbase-dev
	samurai
	sqlite-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/network/kget.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kget-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ad3dcecb675215b0e8256605315d9eaea6f27b8f1b13a63bdd890b23078025502f20ff084e439a50aca801244809fa27bdd6ef5795051ccd099c072be5565070  kget-24.08.0.tar.xz
"
