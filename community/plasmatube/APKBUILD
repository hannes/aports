# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=plasmatube
pkgver=24.08.0
pkgrel=0
pkgdesc="Kirigami YouTube video player based on Invidious"
# armhf, ppc64le, s390x, riscv64, loongarch64: blocked by purpose
arch="all !armhf !ppc64le !s390x !riscv64 !loongarch64"
url="https://invent.kde.org/plasma-mobile/plasmatube"
license="GPL-3.0-or-later AND CC0-1.0"
depends="
	gst-libav
	gst-plugins-good
	gst-plugins-good-qt
	kcoreaddons
	kdeclarative
	kirigami
	kirigami-addons
	kitemmodels
	purpose
	qt6-qtimageformats
	yt-dlp
	"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami-dev
	mpvqt-dev
	qt6-qtbase-dev
	qtkeychain-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/plasmatube.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/plasmatube-$pkgver.tar.xz"
# No tests
options="!check"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dea1f36e4ed697d653eb38ef06a984d84fc6c750cc155913d0af842eb2e54863305faef1b2e02799b958eeaa8a6d8143ea57c39b5a5b42e9462ec6c4f7509642  plasmatube-24.08.0.tar.xz
"
