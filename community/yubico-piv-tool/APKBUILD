# Contributor: Christophe BERAUD-DUFOUR <christophe.berauddufour@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=yubico-piv-tool
pkgver=2.6.0
pkgrel=0
pkgdesc="PIV Tools for yubikey"
url="https://developers.yubico.com/yubico-piv-tool"
arch="all"
license="BSD-2-Clause"
makedepends="
	check-dev
	cmake
	gengetopt-dev
	help2man
	openssl-dev
	pcsc-lite-dev
	samurai
	zlib-dev
	"
subpackages="$pkgname-static $pkgname-dev $pkgname-libs $pkgname-doc"
source="https://developers.yubico.com/yubico-piv-tool/Releases/yubico-piv-tool-$pkgver.tar.gz
	werror.patch
	fix-incompatible-args.patch
	"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr

	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
11efcc12cdcdddc8f6a946460ee8be40d0652478f9884b01945ec048f95e03e706f00737607a20e75135667ece0de06627cd0a4f441d9048ad9e19f3f67fee00  yubico-piv-tool-2.6.0.tar.gz
1475032b9588bb56026a9850a041e7e287502fc53a7efe038c11ea60d719c166199f990a2760ff18b31c57be287825553de76dc79faf59e9d4064bfa8c01b31f  werror.patch
e1169988fc95bb59dd861998b061c3b7bf7d07a3d75a4932be6ded6529b557e3e7f5278ffe603f72b28929d622ed6c5c90758d9044a77ac5639f28432ed7a483  fix-incompatible-args.patch
"
