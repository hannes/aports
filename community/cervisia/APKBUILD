# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=cervisia
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development/org.kde.cervisia"
pkgdesc="A user friendly version control system front-end"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdesu5-dev
	kdoctools5-dev
	kiconthemes5-dev
	kinit5-dev
	kitemviews5-dev
	knotifications5-dev
	kparts5-dev
	kwidgetsaddons5-dev
	qt5-qtbase-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/sdk/cervisia.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/cervisia-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2ffde343f46dda98d1084438ac566fa1b703f7e792b2eeee3d9d52de80449242e0ac4f5b8d40a98d642428bae2ee29db3efc0163a05f5e7cf3f0af8cfe8ec080  cervisia-24.08.0.tar.xz
"
