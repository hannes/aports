# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=halloy
pkgver=2024.11
pkgrel=0
pkgdesc="Rust graphical IRC client supporting IRCv3.2 capabilities"
url="https://github.com/squidowl/halloy"
# loongarch64: fails to build linux-raw-sys & rustix crates
arch="all !loongarch64"
license="GPL-3.0-or-later"
makedepends="
	alsa-lib-dev
	cargo
	cargo-auditable
	openssl-dev
	"
source="https://github.com/squidowl/halloy/archive/$pkgver/halloy-$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	local appid="org.squidowl.halloy"

	install -Dm755 target/release/halloy -t "$pkgdir"/usr/bin

	install -Dm644 assets/linux/$appid.desktop \
		-t "$pkgdir"/usr/share/applications
	install -Dm644 assets/linux/$appid.appdata.xml \
		-t "$pkgdir"/usr/share/metainfo

	local size; for size in 16 24 32 48 64 96 128 256 512; do
		install -Dm644 \
		assets/linux/icons/hicolor/"$size"x"$size"/apps/$appid.png \
		-t "$pkgdir"/usr/share/icons/hicolor/"$size"x"$size"/apps
	done
}

sha512sums="
719867eb5a7f1a0452cb51ddfa3460c61fdc508f4975ad518fadefc7de935a554f8e8486ebefe09bcdf468bd715d122e55d79af0bfa1012b1ee324741a5dd10b  halloy-2024.11.tar.gz
"
