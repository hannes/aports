# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdenetwork-filesharing
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/internet/"
pkgdesc="Properties dialog plugin to share a directory with the local network"
license="GPL-2.0-only OR GPL-3.0-only"
depends="samba"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	qcoro-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/network/kdenetwork-filesharing.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdenetwork-filesharing-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DSAMBA_INSTALL=OFF
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6968372ebd2b59dae49ffaed6e31215aa20c5b3df3147999fcc59d80c101fba399aec01a96802d542572d0e455341425e60e6bc7f381f8cd60dd43e3ae319b57  kdenetwork-filesharing-24.08.0.tar.xz
"
