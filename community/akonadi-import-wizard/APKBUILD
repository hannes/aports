# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akonadi-import-wizard
pkgver=24.08.0
pkgrel=0
# ppc64le, s390x, riscv64, armhf, armv7 and loongarch64 blocked by kmailtransport -> libkgapi -> qt6-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64 !armv7 !loongarch64"
url="https://kontact.kde.org/"
pkgdesc="Import data from other mail clients to KMail"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-dev
	extra-cmake-modules
	kauth-dev
	kconfig-dev
	kcontacts-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kidentitymanagement-dev
	kio-dev
	kmailtransport-dev
	kwallet-dev
	libkdepim-dev
	mailcommon-dev
	messagelib-dev
	pimcommon-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/pim/akonadi-import-wizard.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-import-wizard-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f38c5e8097c976714666e4762e7bfa85d46a953a3f7473fa0b230eac4dd3c9c36d009060d8e26b4f3e1049e0b9ea239b9ffe061d88a202834a2be308d6ad9f92  akonadi-import-wizard-24.08.0.tar.xz
"
