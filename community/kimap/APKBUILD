# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kimap
pkgver=24.08.0
pkgrel=0
pkgdesc="Job-based API for interacting with IMAP servers"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	cyrus-sasl-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kmime-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/pim/kimap.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kimap-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# loginjobtest is broken
	ctest --test-dir build --output-on-failure -E "loginjobtest" -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4ae7f9309c72d1313a768765a08aa07f95a2194e9886b81925e5a5e10b368a2b8aa61fee3f81a54c4caf7855676914448436b58d5a118ffd7afd28996755b2f0  kimap-24.08.0.tar.xz
"
