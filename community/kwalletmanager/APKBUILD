# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kwalletmanager
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/system/org.kde.kwalletmanager5"
pkgdesc="A tool to manage the passwords on your KDE system"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	kauth-dev
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	kstatusnotifieritem-dev
	ktextwidgets-dev
	kwallet-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/utilities/kwalletmanager.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kwalletmanager-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9a8c413e983965592fedc1de95900f2e8b754dd8e06536c2747cfb092078e59a224eb3e2cd8a7a00a43a9add4cc94baa02327f78686f940531f196844e75c73f  kwalletmanager-24.08.0.tar.xz
"
