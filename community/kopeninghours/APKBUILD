# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kopeninghours
pkgver=24.08.0
pkgrel=0
pkgdesc="Library for parsing and evaluating OSM opening hours expressions"
url="https://invent.kde.org/libraries/kopeninghours"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="BSD-3-Clause AND CC0-1.0 AND LGPL-2.0-or-later"
makedepends="
	bison
	doxygen
	extra-cmake-modules
	flex
	graphviz
	kholidays-dev
	ki18n-dev
	qt6-qtbase-dev
	qt6-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/libraries/kopeninghours.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kopeninghours-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DBUILD_WITH_QT6=ON \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# evaluatetest and iterationtest are broken
	ctest --test-dir build --output-on-failure -E "(evaluate|iteration)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a5f71f6c95d9a4ef9c78148ef960893240707a73faa917510a52fe821789cf88cc7bd46af43d9d46f83e36129e13ab4de42fa080198e36c49e6a84830ce39f9a  kopeninghours-24.08.0.tar.xz
"
