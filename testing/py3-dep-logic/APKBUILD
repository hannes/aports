# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-dep-logic
pkgver=0.4.5
pkgrel=0
pkgdesc="Python dependency specifications supporting logical operations"
url="https://github.com/pdm-project/dep-logic"
arch="noarch"
license="Apache-2.0"
depends="py3-packaging"
makedepends="py3-gpep517 py3-pdm-backend py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/pdm-project/dep-logic/archive/$pkgver/py3-dep-logic-$pkgver.tar.gz"
builddir="$srcdir/dep-logic-$pkgver"

build() {
	export PDM_BUILD_SCM_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
e0123d4f0caf64afecd6e200039124b87c2a138ce0ecf3294ebc191d8948016fda3155f6f839b81b3c29e87d8290d9e88083fa77c7dc19e24cf28eb17611679d  py3-dep-logic-0.4.5.tar.gz
"
