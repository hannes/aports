# Maintainer: David Heidelberg <david@ixit.cz>
pkgname=piglit
pkgver=0_git20240826
_gitrev=eeb74a85ac0484f896995d196814db1b247656a1
pkgrel=0
pkgdesc="a collection of automated tests for OpenGL, Vulkan, and OpenCL implementations"
url="https://gitlab.freedesktop.org/mesa/piglit"
arch="all"
license="MIT AND GPL-2.0-only AND GPL-3.0-or-later AND GPL-2.0-or-later AND LGPL-2.1-or-later AND LGPL-3.0-or-later AND BSD-3-clause"
depends="
	py3-mako
"
makedepends="
	binutils
	cmake
	freeglut-dev
	glslang-dev
	glu-dev
	libx11-dev
	libxcb-dev
	libxkbcommon-dev
	mesa-dev
	opencl-headers
	opencl-icd-loader-dev
	py3-lxml
	py3-mako
	py3-numpy
	python3
	samurai
	vulkan-loader-dev
	waffle-dev
	wayland-protocols"

source="$pkgname-$pkgver.tar.gz::https://gitlab.freedesktop.org/mesa/piglit/-/archive/$_gitrev/piglit-$_gitrev.tar.gz"

builddir="$srcdir/piglit-$_gitrev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DSBINDIR=/usr/bin \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DPIGLIT_USE_WAFFLE=1 \
		-DPIGLIT_BUILD_GLES1_TESTS=0 \
		-DPIGLIT_BUILD_GLES2_TESTS=1 \
		-DPIGLIT_BUILD_GLES3_TESTS=1 \
		-DPIGLIT_BUILD_CL_TESTS=1 \
		-DPIGLIT_BUILD_VK_TESTS=0  # disable Vulkan due to multiple deps
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e5addd75a069b335fb6fd7067b1f4964fbfa9cad4c882f8e7e4c453682f4de48c663b2d83e7ea859bb64646722b2032c930f5d08613d30be29cc617b97b5d40e  piglit-0_git20240826.tar.gz
"
